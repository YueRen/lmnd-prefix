#!/bin/bash

ARG="$1"
MYPATH="`dirname $0`"
OLDPWD="$PWD"

DESTDIR="${ARG:=$OLDPWD}"

if [ "$(ls -A ${DESTDIR} > /dev/null 2>&1)" ]; then
	echo "Destination directory is not empty. This might cause corruption."
fi

pushd "$MYPATH"
"./scripts/bootstrap-sage.sh" "${DESTDIR}" tree repo || exit 1

# with git 1.7, it is possible to use "git status --porcelain" to check if
# there are any uncommitted changes (staged or not) or new files. We aim for
# portability here.
if [[ $(git diff --exit-code --quiet HEAD > /dev/null) || $(git ls-files --others --exclude-standard --directory | grep -c -v '/$') ]]; then
	echo "Directory contains uncommitted changes. Applying changes to test tree."
	git diff HEAD | patch -d "${DESTDIR}/dist/portage" -p1
	cp "${DESTDIR}"/dist/portage/scripts/{wscript,configure} "${DESTDIR}"
fi
popd
