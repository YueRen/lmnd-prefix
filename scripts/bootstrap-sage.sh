#!/usr/bin/env bash


DISTFILES_URL="http://www.lmona.de/files/distfiles"
SNAPSHOT_URL="http://www.lmona.de/files/distfiles"
#GNU_URL=${GNU_URL:="http://ftp.gnu.org/gnu"}
GENTOO_MIRRORS=${GENTOO_MIRRORS:="http://distfiles.gentoo.org/distfiles"}
#GCC_APPLE_URL="http://www.opensource.apple.com/darwinsource/tarballs/other"


# This is copied over from the Gentoo prefix bootstrap script
# since we suppress the output from that, we need to handle errors ourselves
# Just guessing a prefix is kind of scary.  Hence, to make it a bit less
# scary, we force the user to give the prefix location here.  This also
# makes the script a bit less dangerous as it will die when just ran to
# "see what happens".
if [ -z "$1" ];
then
	echo "usage: $0 <prefix-path> [action]"
	echo
	echo "You need to give the path offset for your Gentoo prefixed"
	echo "portage installation, e.g. $HOME/prefix."
	echo "The action to perform is optional and defaults to 'all'."
	echo "See the source of this script for which actions exist."
	echo
	echo "$0: insufficient number of arguments" 1>&2
	exit 1
fi

# The variable $ROOT points to the base of the gentoo prefix install
SBASE=${1}
ROOT="${1}/local"


# load Gentoo prefix bootstrap script
# assumes the Gentoo script is in the same directory as this one
SCRIPTDIR=`dirname ${0}`
source "${SCRIPTDIR}/bootstrap-prefix.sh"

do_tree() {
	mkdir -p ${ROOT}
	# we put a fake usr directory in $ROOT
	[[ ! -e ${ROOT}/usr ]] && ln -sf . ${ROOT}/usr
	# create the directory layout
	for x in log portage tmp packages distfiles; do
		[[ -d ${SBASE}/dist/${x} ]] || mkdir -p "${SBASE}/dist/${x}"
	done

	for x in etc {,s}bin var/lib/portage var/log var/db; do
		[[ -d ${ROOT}/${x} ]] || mkdir -p "${ROOT}/${x}"
	done
	# symbolic links to keep portage files in $SDIST/dist
	[[ ! -e ${ROOT}/portage ]] && ln -s ../dist/portage ${ROOT}/portage
	[[ ! -e ${ROOT}/tmp ]] && ln -sf ../dist/tmp ${ROOT}/tmp
	[[ ! -e ${ROOT}/var/tmp ]] && ln -s ../../dist/tmp ${ROOT}/var/tmp
	[[ ! -e ${ROOT}/var/log/portage ]] && \
		ln -s ../../../dist/log ${ROOT}/var/log/portage
}

bootstrap_tree() {
	do_tree
	local PV="20110505"
	# a third argument on the command line can be used to give a local
	# file as the portage shapshot
	if [[ -e "$3" ]]; then
		cp "$3" ${DISTDIR}
		do_portage_snapshot . $(basename $3)
	elif [[ "$3"x == "repox" ]]; then
		git archive --format=tar HEAD | (cd "$SBASE"/dist/portage; tar xf -)
	else
		do_portage_snapshot ${SNAPSHOT_URL} sage-prefix-overlay-${PV}.tar.bz2
	fi
	cp ${PORTDIR}/scripts/{configure,wscript} ${SBASE}
}

do_portage_snapshot() {
	if [[ ! -e ${PORTDIR}/.unpacked ]]; then
		efetch "$1/$2"
		[[ -e ${PORTDIR} ]] || mkdir -p ${PORTDIR}
		einfo "Unpacking, this may take awhile"
		bzip2 -dc ${DISTDIR}/$2 | $TAR -xf - -C ${PORTDIR%portage} || exit 1
		touch ${PORTDIR}/.unpacked
	fi
}

bootstrap_setup() {
	local profile=""
	local keywords=""
	local ldflags_make_defaults=""
	# in lmonade we use the CPATH variable instead of setting CPPFLAGS
	#local cppflags_make_defaults="CPPFLAGS=\"-I${ROOT}/usr/include\""
	local cppflags_make_defaults=""
	local extra_make_globals=""
	einfo "setting up some guessed defaults"
	case ${CHOST} in
		powerpc64-unknown-linux-gnu)
			profile="${PORTDIR}/profiles/lmnd/linux/ppc64"
			#ldflags_make_defaults="LDFLAGS=\"-L${ROOT}/usr/lib -Wl,-rpath=${ROOT}/usr/lib -L${ROOT}/lib -Wl,-rpath=${ROOT}/lib\""
			;;
		powerpc-apple-darwin9)
			profile="${PORTDIR}/profiles/lmnd/darwin/macos/10.5/ppc"
			#ldflags_make_defaults="LDFLAGS=\"-Wl,-search_paths_first -L${ROOT}/usr/lib -L${ROOT}/lib\""
			;;
		x86_64-apple-darwin10)
			profile="${PORTDIR}/profiles/lmnd/darwin/macos/10.6/x64"
			#ldflags_make_defaults="LDFLAGS=\"-Wl,-search_paths_first -L${ROOT}/usr/lib -L${ROOT}/lib\""
			;;
		i*86-apple-darwin10)
			profile="${PORTDIR}/profiles/lmnd/darwin/macos/10.6/x86"
			#ldflags_make_defaults="LDFLAGS=\"-Wl,-search_paths_first -L${ROOT}/usr/lib -L${ROOT}/lib\""
			;;
		x86_64-apple-darwin11)
			profile="${PORTDIR}/profiles/lmnd/darwin/macos/10.7/x64"
			#ldflags_make_defaults="LDFLAGS=\"-Wl,-search_paths_first -L${ROOT}/usr/lib -L${ROOT}/lib\""
			;;
		i*86-apple-darwin11)
			profile="${PORTDIR}/profiles/lmnd/darwin/macos/10.7/x86"
			#ldflags_make_defaults="LDFLAGS=\"-Wl,-search_paths_first -L${ROOT}/usr/lib -L${ROOT}/lib\""
			;;
		i*86-pc-linux-gnu)
			profile="${PORTDIR}/profiles/lmnd/linux/x86"
			# compilerwrapper handles the ldflags for us
			#ldflags_make_defaults="LDFLAGS=\"-L${ROOT}/usr/lib -Wl,-rpath=${ROOT}/usr/lib -L${ROOT}/lib -Wl,-rpath=${ROOT}/lib\""
			;;
		x86_64-pc-linux-gnu)
			profile="${PORTDIR}/profiles/lmnd/linux/amd64"
			# compilerwrapper handles the ldflags for us
			#ldflags_make_defaults="LDFLAGS=\"-L${ROOT}/usr/lib -Wl,-rpath=${ROOT}/usr/lib -L${ROOT}/lib -Wl,-rpath=${ROOT}/lib\""
			;;
		*)
			einfo "Your profile could not be detected automatically."
			einfo "Please contact lmnd-devel@googlegroups.com."
			exit 1
			;;
		ia64-pc-linux-gnu)
			profile="${PORTDIR}/profiles/lmnd/linux/ia64"
			#ldflags_make_defaults="LDFLAGS=\"-L${ROOT}/usr/lib -Wl,-rpath=${ROOT}/usr/lib -L${ROOT}/lib -Wl,-rpath=${ROOT}/lib\""
			;;
	esac
	if [[ -n ${profile} && ! -e ${ROOT}/etc/make.profile ]] ; then
		ln -s "${profile}" "${ROOT}"/etc/make.profile
		einfo "Your profile is set to ${profile}."
		echo "${extra_make_globals}" >> "${ROOT}"/etc/make.globals
		# this is darn ugly, but we can't use the make.globals hack,
		# since the profiles overwrite CFLAGS/LDFLAGS in numerous cases
		echo "${cppflags_make_defaults}" >> "${profile}"/make.defaults
		echo "${ldflags_make_defaults}" >> "${profile}"/make.defaults
		# The default profiles (and IUSE defaults) introduce circular deps. By
		# shoving this USE line into make.defaults, we can ensure that the
		# end-user always avoids circular deps while bootstrapping and it gets
		# wiped after a --sync. Also simplifies bootstrapping instructions.
		echo "USE=\"-berkdb -fortran -gdbm -git -nls -pcre -ssl -python bootstrap\"" >> "${profile}"/make.defaults
		# and we don't need to spam the user about news until after a --sync
		# because the tools aren't available to read the news item yet anyway.
		echo 'FEATURES="${FEATURES} -news"' >> "${profile}"/make.defaults
		einfo "Your make.globals is prepared for your current bootstrap"
	fi
	# Hack for bash because curses is not always available (linux).
	# This will be wiped upon emerge --sync and back to normal.
	echo '[[ ${PN} == "bash" ]] && EXTRA_ECONF="--without-curses"' >> \
		"${PORTDIR}/profiles/prefix/profile.bashrc"
}





einfo "Bootstrapping Sage installation using"
einfo "host: ${CHOST}"
einfo "base: ${SBASE}"


CXXFLAGS="${CXXFLAGS:-${CFLAGS}}"
PORTDIR=${PORTDIR:-"${SBASE}/dist/portage"}
DISTDIR=${DISTDIR:-"${SBASE}/dist/distfiles"}
PORTAGE_TMPDIR=${SBASE}/tmp

export MAKE


TODO=${2}
if [[ $(type -t bootstrap_${TODO}) != "function" ]];
then
	eerror "bootstrap target ${TODO} unknown"
	exit 1
fi

einfo "ready to bootstrap ${TODO}"
bootstrap_${TODO} $*
